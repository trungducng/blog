//import essential modules
var express = require('express');
var app = express();

var bodyParser = require('body-parser');
var multer = require('multer');
var upload = multer();
var cookieParser = require('cookie-parser');
var session = require('express-session');
var morgan = require('morgan');
var jwt = require('jsonwebtoken');
var config = require('./config/config');
//
var apiRoutes = express.Router();

//connect to database:
var database = require('./config/database');
database.connectDatabase();
//

//middleware area
app.use(upload.array()); // for parsing multipart/form-data (for multer.upload) 
app.use(bodyParser.urlencoded({extended: true}));  // for parsing application/x-www-form-urlencoded
app.use(cookieParser()); // for cookie
app.use(session({secret: "abc"})); // for session
app.use(express.static('public')); // Serving static files
app.use(morgan('dev')); //log


//load view
app.set('view engine', 'pug');
app.set('views', './views');
//

//load BlogController:
var BlogsController = require('./controllers/BlogsController');
var UsersController = require('./controllers/UsersController');

//load add-blog-form:
var blog = require('./models/blog');
var user = require('./models/user');


require('./config/routes')(app, BlogsController, UsersController, blog, user, express, jwt, config, apiRoutes);

app.listen(80, function (){
  console.log('Server is now running in port 80');
});